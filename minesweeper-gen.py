import random
import pyclip

n = 9
k = 8

def GenerateMap():
	arr = [[0 for row in range(n)] for column in range(n)]
	for num in range(k):
		x = random.randint(0, n-1)
		y = random.randint(0, n-1)
		arr[y][x] = 'X'

		# - - -
		# - x O
		# - - -
		if (x >= 0 and x <= n-2) and (y >= 0 and y <= n-1):
			if arr[y][x+1] != 'X':
				arr[y][x+1] += 1
		# - - -
		# O x -
		# - - -
		if (x >= 1 and x <= n-1) and (y >= 0 and y <= n-1):
			if arr[y][x-1] != 'X':
				arr[y][x-1] += 1

		# O - -
		# - x -
		# - - -
		if (x >= 1 and x <= n-1) and (y >= 1 and y <= n-1):
			if arr[y-1][x-1] != 'X':
				arr[y-1][x-1] += 1 # top left

		# - - O
		# - x -
		# - - -
		if (x >= 0 and x <= n-2) and (y >= 1 and y <= n-1):
			if arr[y-1][x+1] != 'X':
				arr[y-1][x+1] += 1 # top right

		# - O -
		# - x -
		# - - -
		if (x >= 0 and x <= n-1) and (y >= 1 and y <= n-1):
			if arr[y-1][x] != 'X':
				arr[y-1][x] += 1 # top center

		# - - -
		# - x -
		# - - O
		if (x >=0 and x <= n-2) and (y >= 0 and y <= n-2):
			if arr[y+1][x+1] != 'X':
				arr[y+1][x+1] += 1 # bottom right

		# - - -
		# - x -
		# O - -
		if (x >= 1 and x <= n-1) and (y >= 0 and y <= n-2):
			if arr[y+1][x-1] != 'X':
				arr[y+1][x-1] += 1 # bottom left

		# - - -
		# - x -
		# - O -
		if (x >= 0 and x <= n-1) and (y >= 0 and y <= n-2):
			if arr[y+1][x] != 'X':
				arr[y+1][x] += 1 # bottom center
	return arr
	
def TurnIntoEmoji(arr):
	length = len(arr)
	for y in range(length):
		for x in range(length):
			if arr[y][x] == 1:
				arr[y][x] = ':one:'
			elif arr[y][x] == 2:
				arr[y][x] = ':two:'
			elif arr[y][x] == 3:
				arr[y][x] = ':three:'
			elif arr[y][x] == 4:
				arr[y][x] = ':four:'
			elif arr[y][x] == 'X':
				arr[y][x] = ':boom:'
			else:
				arr[y][x] = ':eight_pointed_black_star:'
	return arr

def ConvertWithSpoiler(arr):
	length = len(arr)
	for y in range(length):
		for x in range(length):
			arr[y][x] = "||" + arr[y][x] + "||"
	return arr

playermap = GenerateMap()
print("Map generated")
print("X represents a mine")
for row in playermap:
	print(" ".join(str(cell) for cell in row))
playermap_emoji = TurnIntoEmoji(playermap)
pmap_spoilers = ConvertWithSpoiler(playermap_emoji)
finalstring = ""
for row in pmap_spoilers:
	for cell in row:
		finalstring = finalstring + cell
	finalstring = finalstring + '\n'
pyclip.copy(finalstring)
print("Copied to your clipboard. Paste it on Discord and send!")
