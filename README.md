# Discord Minesweeper Generator
![Minesweeper generator preview](https://gitlab.com/yuukidesu9/discord-minesweeper-generator/-/raw/main/image_2022-12-06_141522623.png)
## What does it do?

It creates a 9x9 Minesweeper game field and copies it to your clipboard, so you can paste it directly into Discord. Each cell is spoilered, so the user can click each cell to reveal their contents.

## Special thanks

- Owen
- Fiona
- GM
- Tina
- Pemyne
- Golden
